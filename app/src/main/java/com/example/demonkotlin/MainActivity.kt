package com.example.demonkotlin

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    var fullName = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val preferenceReader = getSharedPreferences("com.example.demonkotlin", Context.MODE_PRIVATE)
        fullName = preferenceReader.getString("user fullname","").toString()
        welcmeUser()
    }

    fun goButtonTouched(button:View) {
        val firstname = firstnameField.text
        val lastname = lastnameField.text
        if(firstname.length > 0 && lastname.length > 0 ) {
            fullName = "${firstname} ${lastname}"
            val preferencesManager = getSharedPreferences("com.example.demonkotlin",Context.MODE_PRIVATE).edit()
            preferencesManager.putString("user fullname",fullName)
            preferencesManager.apply()
        } else {
           fullName = ""
        }
        welcmeUser()
    }

    fun clearButton(button:View) {
        fullName = ""
        welcmeUser()
    }

    fun welcmeUser() {
        if (fullName.length > 0) {
             resultView.text = "Bonjour $fullName"
        } else {
            resultView.text  = "Saisissez d'abord votre nom et prénom puis appuyez sur Affichage"
        }
        firstnameField.setText("")
        lastnameField.setText("")
    }
}